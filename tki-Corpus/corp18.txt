IRAK TÜRKMENLERĠNDE ÇOCUK
OYUNLARI
Sawash M. AL. D. ABDULMAJEED
2010

https://tez.yok.gov.tr/UlusalTezMerkezi/tezSorguSonucYeni.jsp

He boru boru boru
Ġçinde var iki qoru
Ġki xoruzum salıĢtı
Birsi qana bulaĢtı
Qan diledim dilimçin
Kemer yoxtu belmiçin
Kemer gedip oduna
Qarğa sıçıp buduna
Qarğa dögü qemiĢti
On parmağı gümüĢtü
On parmağı pük ossun
Demir xana yük osun
Demir xanın nesi var
Top oynuyan oğlu var
Ġnci düzen qızı var
El attım incisin qırdım
ġaha xeber verdiler
Tut ağacın kestiler
ġahtan xeber gelince
Kerkük‟e yüklediler
Kerkük sene ne geldi
Bir xınnalı quĢ geldi
Xınnalı quĢ uçurttum
Yeddi dağı geçirttim
Yeddi dağın çaqqalı
Qıpqırmızı seqqeli

Hecle hecle bixawa
Atlandım düĢtüm awa
Awda bulamac yedim
Mullamda ağac yedim
Mullam yoğurt getirdi
Pissig burnun batırdı
Pissig burnuy kesilsin
Qanaradan asılsın
Qanaranın kiligi
Hecci babam belinde
Babam Hille yolunda
Hille yolu serbeser
Ġçinde wawı gezer
Wawıların balası
Meni gördü qiqledi
Ocağa zırıqladı
Ocağdan yumağ çıxtı
Yumağı verdm Ģata
ġat mene köpük werdi
Köbüğü werdim quĢa
QuĢ mene qanat werdi
Qanatlandım uçmağa
Hec kapısını açmağa
Hec qapsında bir avrat
QrĢarında bir beĢik
BeĢigde war bir oğlan
Onun adı Muhammed
Muhammede salavat…

Helhele werin geline
Deste gül werin eline
Altın kemer bağlamıĢ
Gelin ince beline
Helhele werin geline
Deste gül werin eline
Elinde terezi war
Cebinde çerezi war
Elemnen oynar güler
Menimnen ğerezi war
Helhele werin geline
Deste gül werin eline
Qapının yarığınnan
Tanıram yağlığınnan
Günde yüz geli geçer
Üregi yandığınnan
Helhele werin geline
Deste gül werin eline

“Açın enber ağzını ağzını,
Werin Allah borcunu”.
“Allah bir yağıĢ ele
Dam duwarı yaĢ ele
Emmim oğlu geçende
Kundarasın yaĢ ele”.
Ģeklinde bir yağmur duası edilir.
a-Allah bir yağıĢ ele
Dam duvarı yaĢ ele
PaĢa qizi geçende babıcını yaĢ ele
Vereni xatun ele
Vermiyeni daĢ ele
Çemçeleqizim aĢ istiri
Allah‟tan yağıĢ istiri
Açın anber ağzını
Verin yağıĢ payını
Vereni xatun ossun
Vermiyeni qetir ossun
b-Allah bir yağıĢ ola
Qere aba yaĢ ola
PaĢa oğlu geçende
Qondurası yaĢ ola
Hey çemçele çemçele
Çemçelem yağıĢ ister
Hüseyn‟i yağlamağa
BeĢigi bağlamağa
Verene Allah versin
Vermeyene de versin
Oğlu küreken dursun
Qizi helhele versin
Ver Allah‟ım ver
Yağmururunnan sel
Qoç qoyun qurban
Köpekli xarman186
c-Köse geldi qapını qırdı
Dura dura yorulduğ
Tütünvüzden boğulduğ
Açın amber ağzını
Verin Allah borcunu
Vereni xatun ossun
Vermeyeni qetir ossun187

“Mennen sen yoldaĢığ
Bir qutu qumaĢığ
Tayyaraya mineriğ
YeĢil mektub yazarığ
BaĢqanımız gelende
Ayağına qaxarığ”

“Hecciler hece gider Allah için
Allah‟ın bir evi var bin terece
Ġçinde Ģem‟eden yanar yarı gece
El attım küçük kızın memesine
Büyük kız bir xençer vurdu böbreğime
Böbreğim tuta tuta gettim hece
Hec dedi canuw çıxsın gezme gece”.

“Anna meni oyad gedim ta‟lime
Qapağlı tüfengi werin elime
Dağ ensesinde bir ğeribet war
Elimde kitap yolları aĢar
Canım fida watana kâr
Milletimi dinimi ey igid baĢar”.

“Et-te‟ale fat fat
Ġzzeyun sebi‟ leffat
Dayir ma dayir Bağdad
Ya favzi dayir Bağdad
Tut tut izzeyun”

Pıçı pıçı pıçağçı
Ġgne yiğdim mullamçı
Mullam meni oxuttu
Sarı saçım toxuttu
Sarı saçım elimde
Ġpek yağlığ belimde
Ġpek yağlığ su çeker
Döner havuza töker
Emmim qızı Xedice
Meni yolladı saca
Bilseydim getmeziydim
Enwara girmeziydim
Enwarlar menim olsun
DuĢman qurbanım olsun

“Alaylim bulaylim sap sap salaylim”.
deyince ikinci grubun begi:
“Ne istisen ne istisen sene bulaylim”
der ve birinci gruptan Ģu cevap gelir:
“Ġstirem istirem o gözel …ı”.

“Legleg legleg hawada
Yumurtası tawada
Sakız werrem çeynemez
Götüne çallam oynamaz”

Bu bağda dolanısan
Derdiw mene demisen
Etrafuw dolu gülden
Bir gül mene wermisen
Qurbanam xan gözüwe
Mene baxan gözüwe
Çox sürmeler çekibsen
Ewim yixen gözüwe
Eliwden içtim bade
Ümrüm olsun ziyade
Allaha çox yarwaldım
Salmasın seni yade

“Hecci laklak hawada
Yumurtası tawada
Saqız werdim çeynedi
Bir Ģaq çaldım oynadı”

Heccilaqlaq hawada
Yumurtası tawada
Gögde tak tak ederken
Balaları yuwada
Qere Qızın suçu yox
Yumurtanın içi yox

Eveleme develleme
İğne iğne ucu düğme
Koz ağacı
Kotur geçi
Yerine yürüne
Su içer kurtuna.

Leyle ederem yatasan
Konca güle batasan
Leylev ederem daim
Uykun olsun mülayim
Leyle balam leyle

Pıçı pıçı pıçağçı
İgne yiğdim mullamçı
Mullam meni oxuttu
Sarı saçım toxuttu
Sarı saçım elimde
İpek yağlığ belimde
İpek yağlığ su çeker
Döner havuza töker
Emmim qızı Xedice
Meni yolladı saca
Bilseydim getmeziydim
Enwara girmeziydim
Enwarlar menim olsun
Duşman qurbanım olsun
