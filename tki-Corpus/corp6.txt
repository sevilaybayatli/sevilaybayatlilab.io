‫‪
    HALK ŞAİRİ MUSTAFA GÖKKAYA'NIN
    "YARALI KERKÜK ŞİİR KİTABINDAN"
    AĞIZ SES ÖZELLİKLERİ
    
    SAFA SEDEEQ MAHMOOD QLNCHI
    2019
    
    https://iraqjournals.com/article_173098_28fc81a3e125c4e028a75b7957933b16.pdf
    
    
    Dḭyesen yarālîsan                                    Oḫ dēgdḭ yarāsîna 

    Gözlērἶvden  yaş aḫar                             Aḫtārî  tabiP tapmaz                               

    Gög girḭp ḳärǟlḭsen                                 Derdḭnḭn çarāsîna    

2) Kerkük'em yarālîyam                        8) Ḳurum az 

    Gög girḭp ḳärǟlḭyem                               Cülḥe çoḫtỉ  ḳurum az 

    Gözlere tikân ōldἶm                                Tökἶlen şehid ḳānî 

    Men çünkỉ yarālîyam                             Bin il ḳalsa ḳurumaz                                                                               

3) Kerkük'em  ḳärǟlḭyem                                                          

    Cigerden yarālîyam                                                                    MART 1959 

    El diyer bizḭm Kerkük                   

    Bilmem men harālîyam 

4) Kerkük'ἶm nece Kerkük                   

    Düşptἶ saca Kerkük 

    Allah'tan imdaT diler                              

    Her gün her gėce Kerkük 

5) Kerkük ḳalālî Kerkük                          

    Başî   bälǟlḭ Kerkük 

    Şahraban nārî  kimin                           

    Sıḫıp el ēlḭ Kerkük 

6) Kerkük' ἶn yanar dāġî                          

    Boynỉnda sḭyah bāġî 

    Her gün her gėce gēlḭ                               

TÄVḲḽF YOLỈNDA TÜRKMAN 

1) Gėce yātmîş ḳaḫ dễdḭler                  

2 

481 

   
   
 
 
 
 
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

    Ḳarşî yana baḫ dễdḭler 

    Mindḭrdḭler cėbe mēnḭ                     

    Gören dostlar aḫ dễdḭler  

2) Aman zalîm vỉrma mēnḭ                  

    Ḳollārîmnan burma mēnḭ 

    Ezēlḭnnen ḳedḭrlḭyem                      

    Dost ȫgἶnde ḳırma mēnḭ 

3) Cėbe bastîlar mēnḭ  

    İpten astîlar mēnḭ 

    Aġçtan vỉra vỉra 

    Dilden kestḭler mēnḭ 

4) Çolỉḫ çocỉḫ gözἶ nēmlḭ                      

    Ėvde ḳaldî gėvlḭ ġēmlḭ 

    Yolda vỉran yumrỉḫ sımsıḫ               

    BėT çehrēlḭ sözỉ  sēmlḭ 

    Kimse derdḭm sorabilmez                 

    Hiç bir ehlḭm görebilmez 

    Yaġlîḫ cėbde elḭm bāġlî                     

    Göz yaşlārîm silebilmez 

                                                                                                        RAMAZAN 1959                                                  

1959 ' UN  14  TAMMUZ BAYRAMI 

3 

1) Nene bayram olmādî                      5) Nene yıġlar ḳız yıġlar                          

    Bāḫîm babam gelmēdḭ                      Oġlan yıġlar yüz yıġlar                                                                                

482 

   
   
 
 
 
                         
 
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

    Mēnḭmçḭn zubun çeket                      Birbirḭn ḳucāġlîyîP                              

    Babam nişḭn almādî                           Dḭze vỉrar dḭz yıġlar  

    Nene babam gelmēdḭ                         Nene babam gelmēdḭ    

    Mēnḭm sabrım ḳalmādî                     Mēnḭm sabrım ḳalmādî                                      

2) Gün bāttî  gėce ōldỉ                                     

    Bilmḭrem nece ōldỉ                                                                       TAMMUZ 1959                                                                                 

    Dünya gözἶm ȫgἶnde                             

    Renk renk alaca ōldỉ 

    Nene babam gelmēdḭ    

    Mēnḭm sabrım ḳalmādî 

3) Mene neden aġlısan                              

    Ḳapılārî baġlısan 

    Nişn Türkman kēsḭller                         

    Uşaġlārî saḫlısan 

    Nene babam gelmēdḭ    

    Mēnḭm sabrım ḳalmādî 

4) Gene nenem yıġlādî                             

    Gicērḭmḭ daġlādî 

    Balam Türkman kēsḭller                     

    Ėvde  mēnḭ saḫlādî 

    Nene babam gelmēdḭ    

    Mēnḭm sabrım ḳalmādî 

( 14 ) TEMMUZ ḲÄTLḭ ͑ĀMÎ 

4 

1) Süslēndḭ herkes oġlannan ḳızlar                    6)   Sür sürἶklerler hemen ānînda    

483 

   
   
 
 
 
 
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

    Datlî şarḳılar söyler aġîzlar                                  Teḳētḭ ḳalmaz cümle cānînda 

    Unutỉlmaz aḫ o güler yüzler                                 Bir bir āstîlar ḳışla yānînda 

    Oyānîn gençler oyānîn ḳāḫîn                               Oyānîn gençler oyānîn ḳāḫîn 

    Gėvlḭ yarālî ḳalana bāḫîn                                     Bāşî çāmîrlî türkmana bāḫîn 

2) Küçük körpeler gül ḳucaġlarda                     7)  Çoḫỉ yarālî çoḫỉ da ȫldἶ 

    Saḫlāndîlar aḫ yan bucaġlarda                            Şehidlērḭmḭz taplmaz ōldỉ 

    Çoḫ gençler öldἶ tek ocaġlarda                            Cānlî cansızdan dereler dōldỉ 

    Oyānîn gençler oyānîn ḳāḫîn                               Oyānîn gençler oyānîn ḳāḫîn 

    Bāşî bälǟlḭ Kerküg'e bāḫîn 

                         Gėvlḭ yarālî ḳalana bāḫîn 

3) Nece gençlērḭ parça kēstḭler  

    Otomobilden ǟzḭp bāstîlar 

    El ayaḫ gễdḭp bėlden āstîlar 

    Oyānîn gençler oyānîn ḳāḫîn 

    Bāşî çāmîrlî Türkman'a bāḫîn 

4) Zalım zalımlar ḳḭydḭler cana 

    Kerkük cäddǟsḭ bulāndî ḳana 

    Yan Kerkük ehlḭ ḳal yana yana 

    Oyānîn gençler oyānîn ḳāḫîn 

    Gėvlḭ yarālî ḳalana bāḫîn 

5) Yaralar çoştỉ durỉP ḳaḫtîlar 

    Dört yanlārîna dönἶP baḫtîlar 

    Elˬayġlārîna ip sim taḫtîlar 

    Oyānîn gençler oyānîn ḳāḫîn 

    Bāşî bälǟlḭ Kerküg'e bāḫîn 

ŞEHİDLER KÄRVĀNÎ 

5 

484 

   
   
 
 
 
 
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

1) ˁAtaˬBegḭ'n özἶne           7) Bir yuldỉz var bir ayçî     13) Kemal  ˁAbdîssamad’î                               

    Ḥayrānîydîḫ  gözἶne           Bir oḫ ōỉlP bir yayçî              ˁÄcäP ḳılınç ḳamādî              

    Can vễrdḭ baş egmēdḭ         Sḭnēsḭ serend oldỉ                    Parçālîyîp attîlar                                                                                        

    Namartlārîn sözἶne             Meşhurdỉ Zühėr Çayçî           Ḳızıl ollam dėmēdḭ        

2) İḥsan Beg ḳardaşîmîz    8) Şehidˬoldỉ Seyyḭd Ġenḭ     14) ḤäsiP ˁAli kėçēnḭ                                              

    Āḫîttî göz yāşîmîz               Aḫtrram tapmam onỉ              Çễvἶrdḭler gėçēnḭ 

    Āhîmîz ͑ ärşe çıḫtî               Kimḭn ḳarnında  ḳaldî             Allah Cennet'e yollar                                   

    Ḳärǟltḭ daġ dāşîmîz           Kerkük Oġlỉnîn ḳanî                Bỉ şerbetten içēnḭ 

3) FeḫreddinˬOġlỉ Cahḭt   9) Muḫtar Fuat sen aġla         15) Sen baba neye daldỉv                                               

    YaraP özἶvsen şǟhḭt           Üregḭve daş baġla                     Ne satısan ne aldỉv  

    Bỉ rütbeye erişmez             Sesḭv ḳaldîr ėşḭdeḫ                    Aġlısan göz silmeden                                         

    Nanca çalîşsa zǟhḭt           Cihan cigērḭn daġla                   Sıdıḳ’ım harda ḳaldỉv 

4) Ḥüsėn BegˬOġ lỉ Ḳäsḭm   10) Muḫtar bỉ yol ne yoldỉ  16) Ḫızırˬoġlỉ ˁUsman'a                       

    Çaġîrram çıḫmaz sēsḭm     Gözlērḭm ḳannan doldỉ         Kimse vėrmez ses mene 

    Mēnḭ ḳärä basıptî               Cihat Nihat küçük ḳız           Dad ėttḭm feryad ėttḭm                                          

    Kimde teb ͑em kēsḭm         ˁÄcäP bỉlara ne oldỉ                 Sesim çıḫtî ˁasmana 

5) Nüreddḭn ˁÄziz ˁAttar      11) Şäkḭr çayḫanav ḫoştỉ  17) Şofėr ˁAbdulla  Aḥmet                       

    Sḭtrˬėyle sen ya sîttar              Ḳäynǟdḭ ḳorỉ coştỉ             Ruḥỉve yüz bin reḥmet  

    Sene bỉ zulmỉ ėden                  Şehidlḭġḭn şarābî                Bir beşer can vėrende                                         

    Türbede nece yatar                 Ḥäḳ yolỉnda ne ḫoştỉ          Hiç gȫrἶP bele zaḥmet 

6) Ya İbrahim Ramazan    12) Fätḥulla noldỉ sene      18) Ya Seyyid  ˁAbdulḫalıḳ                                          

    Ḳır ōrỉc  oldỉ azan                 Harda bulāndîv ḳana         Belḭ süpḥēne’lˬḫalîḳ 

    Ėv uşaḫ iftar gözler               Babam Yünüs görsēydḭ      Bỉ zulmỉ kim gȫrἶptḭ                                         

    Bele yāzîptî yazan                  Zalîmlar  nễttḭ mene           Sen gördἶv bize lāyîḳ     

485 

   
   
 
 
 
                   
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

19)  Bilmem ˁAli Beg hānî      25)  Kâzım ˁAbbas'tı Bektaş                      

       Serend ōldỉ hep cānî              Gözlerden āḫîttîv yaş 

       SürüklἶyἶP astılar                  Bỉ zulma kim dayānî      

       Her yėrden aḫtı ḳānî 

       Ne daġ dayānî ne daş 

20)  Ey Salaḥeddin Avçî       26)  Cümˁa Ḳänbär vay sene                    

       Çaḫmaḫ ōlỉptî ḳāvçî              Bulāndỉv ḳızıl ḳana     

       Meḥemed’le ḳuş kimḭn          Ehlētἶv ataşlarda                                

       Ḫalḳ ōldỉvỉz bỉ dāvçî             Ḳāldîlar yana yana 

21) Çaġîrram Ḥäccî Näcḭm'ḭ                   

      Ḳaldır köçeḫ cėcḭmḭ 

      Dabban ḳaldır ḳaçāġîn                      

      Geldḭ şėytan recimḭ                                                                

22) ˁAdîl ˁAbdülmecid'indḭ                    

      Bỉ ne vỉcdan ne dindḭ 

      Yarabbi bỉ zulma baḫ                         

      Yėr gög aġlıyor indḭ 

23) Fätḭḥ Yunỉs neşˁēlḭ                  

      Sen Türkmansan he bēlḭ 

      Parçālîyîp attılar                               

      Ne baş ḳāldî ne ēlḭ 

24) Enver  ˁAbbas yanîva                      

      Kimler ḳiydḭ cānîva  

      Cäddǟnḭn boy boyìnca                   

      Bulāndî al ḳānîva 

NÖBĒTÇḭ 

486 

   
   
 
 
 
  
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

6 

1 ) ˁÄlem tānîr men Kerkük' ἶm             

     Şiş şiş ōlỉP her bir tükἶm 

     Güzel gȫzlἶ şehid yükἶm                   

     Nöbetçḭyem men nöbētçḭ 

2)  Türbedaram şehidlere                   

     Bỉ yatan genç igitlere 

     Asılan o sȫgtἶlere                            

     Nöbetçḭyem men nöbētçḭ 

     Gözlērḭmde kirpḭk çalmaz              

     Sḭneme çarpar ebeT durmaz 

     Bir ḥaldāyîm kimse sormaz             

     Nöbetçḭyem men nöbēçḭ 

3) Daġ dayanmaz bỉ āhîma                      

    Bėl baġlādîm Allāhîma 

    Dayan vatan ah vāhîma                      

    Nöbetçḭyem men nöbētçḭ 

HOYRATLAR 

7 

1) Bir deḳḳe                                                       

487 

   
   
 
 
 
 
 
                                   
 
 
     
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

    Üznἶde var bir deḳḳe                                         

    Ḥaḳ ērḭdḭ Ḳaf Dāġîn                                         

    ĠezeP ėtse bir deḳḳe                                           

2) Bir deḳḳēsḭ 

    Bir ḫālî bir deḳḳēsḭ 

    Dünyānî vėren ėder 

    Felēgḭn bir deḳḳēsḭ 

3) Bir deḳḳeye          

    Aldanma bir deḳḳeye                        

    Adāmîn köskἶn örter     

    Bir torpaḫ bir de ḳeyye    

4) Deḳḳe yėrḭ     

    Dȫgἶlḭ deḳḳe yėrḭ    

    Felek bildiġḭn eder    

    Bir silkin de ḳa yėrḭ                 

5) Gög deḳḳe 

    Ḫälî siyah gög deḳḳe 

    Aḫma yuldỉzîm aḫma   

    Biraz daha gögde ḳa      

6) Gög deḳḳēlḭ  

    Siyah ḫal gög deḳḳēlḭ       

    Havalansa ͑ aşḳ āhîm      

    Hiç enmez gögde ḳālî                 

KERKÜK MARŞI 

8 

488 

   
   
 
 
 
 
2019

ةنسل

2  

:ددعلا

14  

: دلـجملا

ةيناسنلإا ت

اـ
ساردلل /كوكرك ةعماج

جملة 

1 ) Kerkük'lἶyḭḫ Kerkük'lἶ                  

     Dostlar şad duşman yüklἶ 

     Fidayaḫ bỉ vatana                           

     Hem gençler hem de tüklỉ 

2) ˁİraḳta kerkük şehrḭ                       

     Ḳärä nėft aḫar nehrḭ 

     Daş altun torpaḫ gümüş                   

     Sür safa çekme ġēmḭ 

3) Sėvērḭḫ  biz vatānî                            

    Hem ana hem atānî 

    Ȫlἶrseḫ terk  ėtmērḭḫ                         

    Bỉ torpaḫta yatānî 

4) Kerkük ˁİraḳ çırāġî                         

    Fitilsḭz yanar yāġî 

    Ḳısa gȫrἶşlḭ dēgḭ                              

    Dayım gözler irāġî  

5) Gėçērḭḫ yüz bin ili                            

     Terk ėtmērḭḫ bỉ dili 

    ˁİraḳ Türk'ἶ Türkman'ıḫ                    

     Dünyada herkes bili  

( A )                                                     SÖLÜK 

489 

   
   
 
 
 
 
 
 
 
 
                                                     

