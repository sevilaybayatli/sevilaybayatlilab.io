https://tez.yok.gov.tr/UlusalTezMerkezi/tezSorguSonucYeni.jsp

Ayver Kareem
2018
https://tez.yok.gov.tr/UlusalTezMerkezi/tezSorguSonucYeni.jsp

Aç aman bilmez, çocuk zaman
bilmez
Aç at bol olmaz, aç ayıt ol olmaz
Aç ayı oynamazAç ayı oynamaz
Açıkan ne olursa yer, acıyan ne
olursa söyle
Acın imanı olmaz
Acın karnı tox olı, gözü tox olmaz

Açtırma kutunu, eşitme kötünü
Adam adam acuğüne bir avuc tuz
yer
Adam adama gerek olur, iki
serçeden börk olur
Adam adama yük değil can
gevdeye mülk değil
Adam adamdı olmasada pulu, eşek
eşekdı olsada çulu
Adamı yerse tanıdığı kurt yesin
Adamın canı çıhsın, adı çıhmasın
Adamın yere bakanından, suyun
sessiz axanından kork
Adım adım yol olu, damla damla
göl olu
Ağaç borç eder, uşak harc eder
Ağaç yaşken egili
Ağaçın gölgesi dibine düşmez
Ağacın kurtu özünden olmsa bin il
yaşar
Ağır ayağ baĢa değer, yüngül ayağ
daşa değer
Ağız yer üz utanıAğız yer, yüz utanır
Ağlasa nenem ağlar, gerisi yalan
ağlar
Ahmak odu bu dünya için gam yer
mavlam bili kim kazanı kim yer
Akıl düşününce deli vurar geçer
Akılsız başın zahmettini ayak
ceker
Al ağzına baxan qarısını tez boşar
Alçağ yerde yatma sel alı üskek
yerde yatma yel alı
Alçak eşek binmeye kolay, öksüz
çocuk dövmeye kolay
Alış oğlu verişti, dünya karış
karıştı
Alışmış kudurmuştan beterdi
Allah bir kapını bağlasa bir kapını
açar
Allah dağına göre kar yağdırı
Allah deveye kanat verse, dam
divarı yıkar
Allah kerimdi, kuyusı derindi

Allah kozu veri dişsize, pilavı veri
iştahsıze
Allah sağ gözü, sol göze ihtac
etmesin
Alnında şeytan tuku war
Altın yerde paslanmaz, taş
yağmurdan ıslanmaz
Altın yere düĢmekle bol olmaz

Altındakı yatağa başındakı yağa
bak
Altun kaba kan kusum
Altunun kadrını altuncu bili
Amcam dayım herkesden aldım
payım
Anadan doğmak ölmek icindir
Analar neler doguruyor
Arha su gelene kadar, korbağanın
gözü patlarArka(göle) 
Arı olan yerde balda vardır
Arpa eken buğda biçmez
Arpa samanı ile, kömür dumanı ile
Arvad var ev dayağı, arvad var
incana boyağı
Aşı pişiren yağ olur, kiĢinin yüzü
ağ olur
At almadan, ahur yağtırır
At beslenirken, kız istenirken
At bulunu meydan bulunmaz,
meydan bulunu at bulunmaz
Atdan düĢmiyen atlı olmaz
Ateş düşen yer yanar
Atı at yanında bağlasan ya
tekmeye ya çifteye
Attan düşmeğ eĢĢekten düĢşmeğden
daha eyyidi
Av kuşu dimdiğinden bellidi
Avcı yatar, köpek otlar
Ay işiğinde herhizliğe çıhar
dile destan olur
çarşambadan bellidir
Az tamah, çoh ziyan getiri
Az yaşa çok yaşa akıbat geli başa
Az yiyen az uyur, çoh yiyen geç
uyur
Azıcık ışım ağırısız başım
Bağa bag üzüm olsun, yemeğe
yüzün olsun



Bal toplayan arıların iğnesine

Balığ baĢtan kohar
Bar alırsın musuldan, kız alırsın
aslından
Başın sağlığı dünyanın varlığı
Bayğuş verane bekler
BaykuĢun rızkını Allah veri
Bayramda borç verene, ramazan
kısa geli
Begenmediğiv daş, baş kırar

Bekerin parasını it yer, kapısını bit
yer
Bilbilinin dili başına beladı
Bilene bir bilmeyene bin ohu
Bilene bir, bilmiyene bin ohu
Bin düşün bir konuş
Bir çulum var atarım, harda gelmiĢ
yatarım
Bir dağ yıhılmasa, birde ere
dolmaz
Bir daşdan iki kuş vurmağ
Bir dinsizin hakkından bir imansız
geli.
Bir elin capğını cığmaz
Bir evde iki kız, biri
Bir koltuğa sığmaz iki karpuz
Bir peştamal var kırmızı hem
nenesi bağlar hem kızı
Biri yer biri bahar onda kıyamet
kopar
Bişmiş aşa su tökiri
Biz çıktığ herhizliğe, ay çıhtı
eglenmeğe
Borçtan alınan eşşek, su yolunda
ölü
BoĢ sözle torba dolmaz
Boyüg baĢ devletti, boyüg ayağ
nekbetti
Bugünki yımırta yarınkı tavuğdan
iyidi
Bugünün yımırtasını yarının
tavuğuna değiĢmem
Cağırılan yere arınmaya,
çağırılmayan yere görünme
Cahalat yarım bilgiden daha iyidir
Çakal var baş kupardı kurdun adı
yamandı
Çalma haxın kapısın çalmasınlar
kapıvı
Camı mollasız olmaz
Can çıkar huy çıkmaz
Can çıkmayınca huy cıkmaz
Çayı daşıydan, çay kuĢu vururı
Çayır, öz kökü üzerinde biter
Cefa çeken safa sürer
Cefanı çekmeyen sağın kadrını
bilmez
bilmezÇem çakalsız olmaz
Çibin küçükdi, ama mide bulantırı

Çıhmıyan canda umud var
Çırağ dibine Ģüle vermez
Çoh gezen çoh bili, çoh yaşıyan
çoh bilmez
Çoh veren maldan, az veri candan
Çok söz var kılıctan keskindir
Cömert derler malden ederler, iğit
derler candan ederler
Cülhe bezi tapmaz
Dağdakı geli bağdakını kavar 
Danışan dağı aşar, danışmayan
yolu şaşar
Danışmağ için büyük adam
bulmasaw büyük taş bul
Daş yerinde ağırdı
Davulun sesi uzağından hoştu
Değirmen ettiğini eder, çıkçığa baş
ağırdı
Değirmen iki taştan muhabbet iki
baş
Değirmen satıl suydan dolanmaz

Değme sarhoşa, özü düşer
Deli deliden hoşlanır, molla
hacıdan hoşlanır
Deliyden çıkma yola başa gelir
dürlü bela
Delli bazar, zor bazar 
Demir doküle doküle polat olu
Demir ıslanmaz, deli uslanmaz
Demir kapının ağaç kapıya işi
düĢer
Demir tavunde gerek

Denguzdan korhan darı ekmez
Denguzden korkan darı ekmez.
Deniz dalgasız olmaz, kapı
halkasız olmaz
Denizdeki balığın pazarlığı olmaz
Denize düşen ilana sarılı
Denkuzden korkan darı ekmek
Derdin yoksa söylen, borçun
yoksa evlen
Derdini sağlayan dermen bulmaz
Derviş paraydan bitirir her işini
Derya dalgasız kapı halkasız
olmaz
Deve öz kamburunu görmez
Deveciyden kalkıp oturan kapısını
büyük yapar
Deveciyden konuşan kapısını
büyük eder
Deveye dediler boynuv eğridi,
dedi haram düzdü.
Deyve öz kanburunı görmez
Deyveden böyük fil var
Deyveni bir cigerçin öldürri
Deyveye dediler boynuv egiridi
dedi haram düzdi
Dil kesik baş selamet, dil uzun baş
melamet
Dilim, seni dilim dilim diyellem,
her başıma geleni senden billem
Diliw yorulduğca eliw yorulur
Dişiv ağırdı çek kurtul, başıv
ağırdı göç kurtul
Doğrı diyenin börki dellük olu
Donguzdan bir tük kupansın he
kardı
Dost başa bahar, düşman ayağa
Dost meni bir kozdan istesin koy
oda pük olsun
Dostu göstermekle düşman
azalmaz
Dostuna dost düşmanına dusman
Düğün olur iki kişiye, kavgası
düşer deli komşuya
Düşenin yoldaşı yohtı
Duvarı nem yıkar insanı da gam
Duvarın kulağı var
Ecel geldi cihana, baş ağırısı
bahane
Eğiri ağac düz olmaz rande
vurursaw yüz yerden
Eğiri otu düz seleş
Ekrabayden ye, iç, alışveriş etme
El elden üstündü
El elden üstündü (dür)
El eliyden iş etme
yapış
El istesem ellisi geli göz istesem
tellisi geli
Eldeki köpek, çemdeki aslanan
eyidi
Eli yağ baldadı
Elim vurdum özüme özüm ettim
özüme.
Elin ağzı torba deği büzsun
Elin ağzına baxan aç kalı
Adam işlemeyince dişlemez
Torpağı işleyen ekmeği dişler
Adam ondı, dokuzu dondı
Adamı kilinç öldürmez bir tehne
söz öldürü
Ağa bağınnan, su bağışlırı
Ağırlarda göz ağırısı her kişinin öz
ağırısı

Akıl işine bakar cahil deşalıda gül bitmez, 
Atlı atlıya eş olmaz, arpa ununden
aş olmaz
At ara oya
Atın her yorulduğu yerde han
yapılmaz
Balcının var bal tası, oduncunun
Çıhan ay çıhışınnan bellidi
Dil öğünde sümük yoxtur
Eldeki köpek camdakı aslannan
eyidi
Eşek o eşekti palanı değişip
ildan Aydan bir namaz, oda hakka
yaramaz
Mahkeme kazıya mal olmaz
Elin torpağa vursa torpağ altun olu
Eliw boşsa sahat, paraw çoxsa
kefil oli
Eliyden haç Kabul olmaz
Emanet ata minen tez ener
Er ise meydana cık
Eski pambuğ bez olmaz, eski
düşman dost olmaz
Eşşeğin canı yansa atından fazla
kaçar
Eşşek baklavadan ne anlar
Eşşek o eşşekdi, palanı degişip
Et dırtağtan ayrılmaz
Eyiliğe eyiliğe her adamın derdidi
kötülüğe iyilik merdiyiliğe 
Fakırın ahı tahtından indiri şahı
Fazla aş karın ağırdı ya baş
olu
Felek insana her zaman yar olmaz
Garıp dostu olmayandır
Garıp kuşun yuvasını Allah yaparG
Garıpın parası bol karısı duldur
Geceler gebdır gün doğmazdan
neler dogurur
Gecen gecti gelene bak
Geçme namert köprüsünden koy
aparsın su seni
Gençe varan yumruk yer, ihtiyara
varan koyruğ yer
Gergi öz sabını kesmez
Gevur ekmeği yen gevur kılıçı
çalarGavurun ekmeğini yiyen,
gavurun kılıcını çalar
Gewilden gewile yol war
Gewlin sevdiği sultandı

Giden bağdan gider, bağvandan ne
gider
Göz görmez, üz utanmaz
Gözden irağ olan gewilden irağ
olu
Gül ağacınnan odun olmaz gül tikansız olmaz
Gülden tikan tikandan gül
Güler yüz altun anahtardı
Gülme konşuva geli başıvaü
Gülün kadrini bilbil bili
Gün gider bela gitmez
Güneşi germeyen o hekim girer
Güvenme dostuva saman dolduru
bostuva
Güvenme varlığa düĢesen darlığa
Hak diyerken su durur
Hak gelince akan sy durur
Hak verirse sormaz kimin oğludu
Halının tozu tükeni delinin sözü
tükenmez
Hamam suyuydan balığ tutar Hançer yarısı sağalı, dil yarası
sağalmaz
Haramdan gelen harama gider
Haydan ne gelup gider de hoya
Hayvan ayağınnan, insan dilinnen
bağlanı
Hekimden sorma çekenden sor
Heleb ordaysa arĢunı burdadı
Helve biĢ ağzıma düş
Her bulatta yağıĢ olmaz
Her bulutdan yağıĢ yağmaz
Her sekkelin, bir tarağı varHer sakalın bir tarağı var
Horuz baynamasa de sebeh olu
Horuz da öz güllüğünde baynarher 
Horuz vakktında baynar
Horuzu çok olan köyün sebehı geç
açılı
geç oluriğnesin yen, çuvaldız sıçar
iğneyden kuyu kazırı
iki gevil bir olsa samanlık seyran
olu
iki karpuz bir elde tutulmaz
ilden Aydan bir namaz, oda hakka
yaramaz
insan insanın şeytanıdır
işleyen dişler, işlemeyen dişlemez

işten artmaz, dişten artar
ister görünen köye kılavuz
it hurur kervan yürürit ürür kervan yürür, yiğit
ölür Ģan(namı) kalırĠyi evlat malı nilir, kötü evlat malı
niliriyi evlat tutar ananın malını,

iyilik et at deryaha, balık bilmezse
halık bilir
Ilan yarpızı sevmez gider burnu
önünde
Kaçanı tut kalan malımızdır
Kaldır samanı, gelir zamanı
Karğa besledim gözüm çığarttı.Besle kargayı oysun gözünü.Karın kardaştan azizdi
Kazan dinkirleni, kapağını tapar
Kazana ne bokulursa cemceye o
gelir
Keçinin koturu bulağ başınan su
içer.
Keskin sirkanın zararı öz kabınadıKeskin sirkenin zararı
küpünedir
Kişinin hürmeti de zilleti de kendi
elindedir
Kılıç öz kabın kesmez
Kırılan cam yerini tutmaz
Kıskanan göze kıl düşer

Kıskançlık şüphe tarafından
beslenir

Kızını vurmayan dızını vurar
Komşusu için kuyu kazan öz
boyuna göre kazmalıdır 
Komşusuna ümidli olanın ekmeğı
doğrağlı kalır
Komşuya umut bağlayan,
kandilsiz yatar
Kör kör içinde olsun ev ev içinde
olmasın
Kör köre diyer ceret gözüve
bulamac ile sürt gözüve
Karga kargaya diyir üzüv
karadı
Kör ne ister iki göz biri eğiri biri
düz
Korkunun acele faydası yoktur
Koyun bulunmazsa keçiye çelebi
diyerler
Kozu verip dişsize, pilav verip
iştahsıza
Kurdun davatına giden köpegini
beraber almalıdır
Kurt tükünü değiştiri huyunu
değiştirmez
Kurttan yer, koyunan şivan eder
Kurtun topası koyunu görüncedi.
Kuyunı kazan düşer
Lalın dilinen nenesi annar

Mal sahıbına benzemezse haramdı
Malını it yer canını bit yer
Malıw mal olmasın pazarıw pazar
olsun
Malıwdan haberdar ol, konşuvu
hirsiz etme
Maşa varken eliv yandırma
Misafir misafirden bezar, ev sahibi
kissinende bezar
Miskinin ahı, tahtden endiri şahı
hazırlamalıdır
Nardıvana ilk bayadan cıkılı
Ne şaytanı gör ne kulvallahu ahd
oku
Ne şeytanı gör ne kulvehad oku
Oğlanın igidi dayısına çeker
Ölü eşşeğın nalı cekilir
Ot kökü üste biter
Oynamiri bir ipde iki canbaz
Para paranı kazanı
Parası aziz olanın, özü zelil olu
Pisigin burnı ete yetişmez diyer
kohuptı
Sağ elin verdiğini sol el görmesin
Sağlık varlıktan iyidir
tahınla deve olunmaz
Saman altında su yeridi sepet
altında iş görür
Saman dıyar kaldır meni
yaramazsam yandır meni
Sanatı ustasından öğrenmeyen
öğrenmez
Sen ağa men ağa, inekleri kim
sağa
Sen isitsen benim için men öllem
seniwçin
Serhoşa değme, özü yıhılı
teytanı aldatır
şirin dili ılanı deliğinden çıkarar
Sirke nanca keskün olsa ziyanı öz
kabına
Sögme kul babamı, sögmiyim beğ
babavı
Son pişmanlık el vermez
Sora sora hece gidilir
Söylemek asanttı yapmağı
zahmattı
Su cerresi su yolunda kırılır
Su damlaya damlaya göl olur
damlacıktan sel olur
Sürüden ayrılan koyunu kurt kaparS
Tahilsizi deve üstünde ılan çalar
Talihin yar olmazsa başta egikiniw
geli gelir daşta
Tavuğun gözü he güllühtedi
Tek olsa elin sesi cıkmaz
Tembel koyun sırtındakı yünü ağır
sanır
Tenbel insani şeytanın oyuncağıdır
Tırnak etinden ayrılmaz 
Tohun, acdan haberi yohtı
Ucuza alınan pahalıya mal olu.
Uşağım yoh eteğim poh
Üsti kalaylı, altı vavaylı
Utananın oğlu olmaz, olsada hirin
görmez
Vahıtsız baynıyan horuzu keseller
Var evim karam evim, yok evim
veren evim
Vursav öldir, yedirtse doydır
Ya bu derdi çekmeli, ya bu köyden
köçmeli
Yaşda kurı ayağına yanar
Yavru kuşun ağzı büyük olu
Yel eken fırtına biçer
Yel kayanın neyini aparı
Yen bilmez, doğrıyan bili
Yetimin ahı, indirir şhı
YoldaĢ tanı yola var, yolda yüzbin
bela var
Yorğanıva göre ayağu uzat
Zehirden şifa eklenmez
Zenginin parası, fakirin çenesin
yorar

Açuğ geli üz saralı, açuğ gider 
üz karalı 
Adam her düĢmeğde, bir akıl 
kazanı 
Adam öz gözüne körlığ istemez  Kimse kendine zarar gelmesini istemez 
Adamın öz eli öz kissesi 
Ağ karganın iti bilinmez 
Ahlak paradan evladı 
Alış oğlu veriĢti dünya karıĢ 
karıştı 
Alıştı yağlı dolmaya, aceb bir 
gün olmaya 


Allah yıhmadığı evi bende 
yıhmaz 



Allahın küçük sabrı kırık yılda  

Altından vurar babuzu dayan 
dur ammı kızı 



iyi  şey  değildir. 


Arbab olan aldanmaz 


Arvadıw kaburga sümügü 
kimindi düzeltmeğ istesew kırılı 




 
 
 
 
At, atlını tanır 



Avvelinden şeytan el uzar 


Aydan bulur, aylanın olur 



Ayı, gördüw tut, ayı gördüw yut 

 

Bablı babına, çemçe kabına 



Ben bağdakı pişmemiş üzüm 
sandım, meğerse bu da 
şişmemiĢ üzümmüş 

Biğden kaldırdı, sekkele koydı 



Bilbil besledim başıma karğa 
çıhtı 



Bilbil öz günü için ağlar 



Bir buğda üzünden, bin zivan su 
içer 



Bir kereyden mınara baĢına 
cığmag olmaz 



Bir yerden yaralıysam, yüz 
yerden kanım ahar 



 
 
 
Biz razı olduğ kaza kaz götün 
koydu naza 

Bizim horuz siziw fareyden 
aşnadı 

Bu xemir çoh su aparı 



Candan sora cihan harab olsun 



Cemidan hesir umusan 



Culun Sudan cığartta bili 



Çemde çakal azdı, bu da geldi 
üstüme 




Çi eşşegi apar yük yanına, çi 
yükü geti eşşek yanına 

DaĢ değene bellidir, yol da 
gidene 

Daşı at sahabı kaldırı 



Dedim adam ol, demedim 
kadam ol 



Deve irağına, katır dırnağına 
bahar 



Devenin iğne dellüğinden 
geçiriri 



Deveye sor diyerler hardan 
gelisen dedi hamamdan 



 
Dokuz gün şivan onuncu gün 
Ģölen 


Duvar düşende toz koyar 

Duvarlı bağa girmez 

Düşmana silah gerek, ya 
düĢmandan ırağ gerek 


El ali … kim eli öz alım hekim 
eli 

 

El eli tanır 

El vurma şeytan şerdi 

Elinde taş apardı oyuna yas 
apardı 


Eşşege minip eĢĢeğı axtarrı 



Gemide oturup gemicinin 
gözüni çıhardırı 



Gırat her müskileye galb eder 


Gör öz sapını kesmez 



Göz yaĢıda borçttandı 


 
 
 
 
 
 
 
Gülmeyi sevenin dişleri 
beyazdır 



Gün gider bela gitmez 


Günü güne bağışlar 

Her günü yaşamak gerek 

Hacı laklakın göwli taktaktan 
hoştı 



Hanadan dan hata cıkmaz 



Hara gittim arpa ekmeği balığ 
başı 

Yapılan iĢlerde rızkının kısıtlı olması 

Ilan kimin ağar akrap kimin 
sokar 


Ilanın ağına karasına lanet 



Kan kana kardeşdi 



Karpuzu yen kurtuldu kapuğunu 
yen tutuldu 



Katır dırnağına bağar devede 
uzağına 



Kaz kazdan baz bazdan gecel 
tavuğ gecel horuzdan 



Kazan üç daĢ üzerinde durar 


Kısrağı genç gözüyden kızı 
ıhtıyar gözüyden al 



Kimin atı ölu kimin iti bayram 
eder 



Kimine aksam gölgesi kimine 
sabah gölgesi 



Kimse öz gözüne körlığ istemez  Kimse kendine ziyan gelmesini istemez 

Köpeğe sümük verirler sahıbı 
xatırı için 



 
Köpek bayram sümügüyden toh 
olmaz 



Kör eline bit düĢüp 



Kör göz ıĢığ sevmez 

Kör yesin köse sıcsın 



Kör yolu kimin görikiri 



Körden vayı alem pilav baklava 
yiri 



Körün tutuşu karın vuruĢu 



Kurdun topası koyunu görüncedi  



Mal ardı gediri 



Mal malak mal haramak 



Malı Gideni imanı da gider 



Nagırcilik edirdi ekmeğ yemeğ 
ayıbına gediri 


Ne kuzu oldum süte toh oldum 
ne koyun oldum ota toh oldum 




Ne malın wafası var ne husnuv 
gün 

Ne sögütte bar, ne kahpede ar 

Nenem ellere satsın, babam 
ellerden alsın 


 
 
 
 
 
 
Nenesi ögen kızı, meğer dayısı 
alsın 



Oğlanın sekkeli çıxtı sen sekkeli 
taraĢ elle 



Olma tokyan olursey aryan 

Ölü atın nalını çekiri 



Özüne ölüm, sahabine zarar 



Pissikde kavurğa yermiĢ. 



Seçe seçe düĢtüm hiçe 



Söz götürür, dorda oturur, 
götüremezsen çolde oturur 




Su karbilde ölçülmez 



şenbe yahudi boynundadi 



şey tanı şey sat şey al şey tanı bir 
bire hizmet eyle olmaginen 
şeytanı 

şeytan diri böyle yap ve Ģöyle 
yap 



şeytan toku boynuva geçsin 

 

şeytana azdırmak 



şeytana kilav girdiri 



şeytanın yatmadığı yeri tanır 



Taş kırılsın, ziylemesin 



 
Taşı at sahabı kaldırı 



Tavuğa kiş diyeceği çal ayağını 
kır 


Tavusu tükü için keseler 

Ġyi  sıfat  olan  insan  her  zaman  baĢkaları 
tarafından zarar görür 

8/7 

Tülkiye dediler sahadıw kimdi 
dedi kuyruğumdu 



Utananın oğlı olmaz, olsada 
xerin görmez 

Ver niyaz al mıraz 



Yağından yarma kavrulur 
arasında soğan buruĢur 



Yen bilmez doğrayan bili 



Yıhılan duvarın, o dekke tozu 
çıkar 



Yüz derenin başını bir axlan 
keser 



Yüz serce bir kazan 
dolandırmaz 

Yüz  adet  serçe 
tüy  ve  kirlerinden 
temizlendikten  sonra  çok  küçük  kalacağı 
için bir tençereyi dolduramaz  

Zengini baxar iĢine yoksul 
baxar dişine

Kimin anası var büyük, qalası var. 

Yoldaşın tanı yola var, yolda bin bela var. 
Qarın qardeĢten ferzdir.
Altın kepeğe ihtaç olur. 
Ya mert ol meydana en ya bir merde hizmet ele. (Y
Kan suya dönmez.

Oğlum oğlumdu hatta evlenince, kızım kızımdı hatta ölünce.
Kızlar erkeklere göre daha vefalıdır.
şalan şırt palan yırt
- Palan kopandıgı halde hayvan hiç birşeye yaramaz. 
- Deyirmenin ip kırıldığında işe yaramaz hale gelir.


Aç göz hiç toh olmaz
Ağacın gölgesi dibine
düĢmez.
Akıl başta değil ama yaş başa
getirir
Akıllı kimseyi falcılar
sevmez
At ne kadar zubun olsa gene
çörek üzerindedir
Atanın sözini tutmayan
vaydan uzak olmaz
Atlısı olanın devesi itmez
Avcı yatar köppek otlar
Bağda olmasa gülümüz bizde
ne kıymetı var
Bağvan bekliyen bağda bar
tapılır
Bin baş bir torbaya bir baş bir 
Boş boğazın başı belalıdı
Bilbil bağın bezegidir
süsler ve bezer, insanın meyli çocuk ev
bezegi ve tatlı sesleri
Dil ögünde sümüğ yoxtur
Duman ağdan ne
kopartacaktır
Dünya arı kabir azabından
çoxtu
Duvar düşende toz kopar
Duznan ekmek düşman
olmaz
Eskiden bek olmayan
yengiden bek olmaz
Gezen tilki yatan aslannan
iğittir
Gülden tikan, tikandan gül.
Harda aş orda baş
Hatın eğrisi büyük oküzde
Her ağaçdan odun olmaz
Her ağaçın barı olmaz
Her huren köpeğe daş atarsan
daşın tükenir
Hordamağın bilmeyen köyün
sürüsüne kurt getirir
Kan kana karıştı.
Qavan qavılır
Kimine akşam gölgesi
kimine sabah gölgesi
Korkmayan korkutmaz.
Koy seni tanıyan kurtlar
yesin
Kurtla köpeği tanımayan
çoban çoban değiS
Kuyu kazan kuyudan
korkmaz
Olma tuğyan olursay üryan
pişmiş boğazdan çıkan söz
kimseyi incitmez
Put perestten korkmamızdan
kork
Sıcandan olan dağarcuğ
dibisi delinir
Sırrına yetik olmayan dost
dost değildir
Söylemeğ asantı, yapmağ
zehmetti
Süretin inandığı aynadır
Terazi hakkın nazargahıdır
dırnağ etten ayrılmaz
Toz kervanın yoldaşıdır
Tutunden kaçan oda düşmez
Ucuz et kazan dibi delir

Yüz dereninin başını bir
axlan keser
Yüz serçe bir kazan
doldurmaz

Adımını tek atmak
Ağaç başında
Alt dudağı yer süpürrü üst
dudağı gök
Amanı gizlemek
At kuyruğu yağmağ
Atından kısrağından atından bu
yolu yayan gideğ anağ şeytan
atından
Ay ışığında hırsızlığa çıkmak
Ayağa çalmak
Ayağının altında yumurta
olmak
Baş beyin aparmağ
Baş çıkartmağ
Baş düşmek
BaĢ kuşamak
Baş oğlan
Başı gitse namazı gitmez
Başına antdiçilir
Başında birşeyler dolaşmak
Başında çıkmak
Başından yeşil tutun çıkmak
Başını açmak

Başıw düz bağla
Başıw top öğüne düşsün
Başıwda çıksın
Boğazından tüklü tavuk geçmez 
Boğazını çıkarmak
Boyacı köyü
Bu başı eliwden gömesen
Çingene borcu
Çık söz batarlı söz
Dilinden asılsın
Diline ulaşmak
Dişi batmak
Dudağından kan damlamak
Dudağı ibinmek
Dudak kaçırmak
Dudak sallamak
El ayak çalmak
El vurma şeytan şerdir
Elifi elifine-
Eliwe versinler başıwa çalsınlar
Evvelinden şeytan el üzer
Fobiyesi çıkmak
Gele götürür gibi
hıxr başına ip takmak
insan insanın şeytanıdır
Kalbini dinlendirmek
Kameti azdırmak
Kapı kapamaca
Kapısını yapmak
Kırşı kırmak
Kör düşi
Kuş dımdığın batır
Kuş gecse kanat döker

Mazi baş
Suna neğmesi
Oyunlar başı
Pis baş
Şaklavadan kaz gelip
Septini kaldırıp civcivleri
çıkıldatmağ
şey tanı şey sat şey al şey tanı
bir pire hizmet eyle olmağınan
şeytanı
şeytan diyiri bele ele veya
şöyle yap
şeytan toku boynuva geçsin
şeytana azdırmak
şeytana gılav girbedi
şeytanın yattığı yeri tanır
Söz çalmak
Söz ekletmek
Söz haket
Söze gitmek
Sözün şekerdenGüzel
Tavuk ayağı acabilmez
Xim xim ile burunsuz,

Yumurtayı bekleyip öğüne
koymak


