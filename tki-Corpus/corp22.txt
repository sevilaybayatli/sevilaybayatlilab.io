IRAK TÜRKMEN KADIN EDEBİYATÇILARI
ZEHRAA HAVAL ISSA 
2019
https://tez.yok.gov.tr/UlusalTezMerkezi/tezSorguSonucYeni.jsp

Merda kurban
Koç kurban merda kurban
Canım ruhım, aşiretim
Verarem merda kurban
Merda mane
Kurban ver merda mane
Mal kimin satılmadan
Ulaştır merda mane
Bu toprağın ne tuzu var
Her baharın bir yazı var
Her bebeğin pak sözü var
Lâl edisen çare ne di’m

Altın Köprü dört bucak
Gençleri kucak kucak
Kahraman vatan için
Bilmezler soğuk sıcak
(Kalaa) hasa ustadı
En güzel bir bestade
Kerem Aslı derdinden
Gece gündüz hastade
leylanda sayanır bülbül
Yahyava sarı Sünbül
Telafer sana kurban
Gelmeşem derdimi bil
Erbil’imin dağları
Yem yeşil bu dağları
Üzümü çok tatlıydı
O tazenin bağları
(Şirikan) ((kara yatak))
Alma, ayva, nar, satak
(Irak’a) yan bakanı
Soyuna zehir katak
(Tavık) sel gibi akar
Yengice tuza bakar
Kara tepa, mendili
Düşmanın evine yakar
Bastamlı bayat yeri
Kümbetler ğirat yeri
Kifrede (aşaa) bağı
Hanakin Hizmet yeri

Dağ olsa
Bu tapeler dağ olsa
Men sevdadan dönmerem
Ataş olsa, dağ olsa
Bir günahım
Af olmaz bir günahım
Yarı yakar kül eder
Tutarsa bir gün ahım

Sen gideli gözler nemde
Dargın gönlüm dolu gamdı
Aslı sevdası keremde
Köylü kızı köylü kızı
Özı gitti kaldı izi
